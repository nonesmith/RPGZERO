# RPGZERO
A character driven RPG based on Dungeon World

## Motivation
Us here at the Guild of Nonesmiths have spent many hours of our lives sitting around a table playing Dungeon World.
In this time we have grown to love a lot of the aspects of the game.
We wish to create a game that contains everything we love about Dungeon World---the fiction based gameplay, the straightforward rules, and the GM style---while improving where we felt it fell short.

## Principles
### Character Driven Narrative
Players and GMs should not only be encouraged to create interesting characters but also be given the tools to do so.

### Narrative Driven Play
Play should focus on telling an exciting, interesting story above all else.

### Continuous Roleplay
As long as you are playing you should be roleplaying.

### Hackability
Players and GMs should be helped and encouraged to create their own content for their games.

## Rough Roadmap

### Core Rules
This includes GM moves, moves for all player characters, health, stats, and other rules that apply across broad spectrums of the game.

### Classes and Races
This includes creating classes and races for characters as well as tips for others to create their own.

### Items and Monsters
This includes creating items and monsters as well as tips for others to create their own.