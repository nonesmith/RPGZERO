# Contributing

Open a pull request explaining the changes and clearly justifying them (e.g. tying them to existing issues).

Overall contributions will tend to fall into one of the three following categories:

* **Content Modifications**

  These modifications will require approval from all of us and be subject to the most scrutiny of all these categories.
  If we do not accept the proposed changes, we will explain our reason.
  We might also approve the changes on the condition of small revisions.

* **Code Modifications**

  These modifications will require approval from all of us.
  If we do not accept the proposed changes, we will explain our reason.
  We might also approve the changes on the condition of small revisions.

* **Minor Modifications**

  Only one of us needs to review minor changes to either accept or reject them.
  We may not always comment on why we reject these changes.

## Style

There does not currently exist a style guide but one may exist in the future.