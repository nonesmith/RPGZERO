{-# LANGUAGE TemplateHaskell #-}

module ItemBox (Item (..), itemBox, Move (..)) where

import Control.Lens

import Text.LaTeX
import Text.LaTeX.Packages.TikZ.Custom

data Move = Move
  { _moveName :: Text
  }
makeLenses ''Move

data Item = Item
  { _itemName        :: Text
  , _itemDescription :: Text
  , _itemMoves       :: [Move]
  }
makeLenses ''Item

itemBox :: Item -> LaTeX
itemBox i = tikzpicture $ draw $ Node (Start $ pointAtXY 0 0) [NodeRight] $ texy $ view itemName i
