{-# LANGUAGE OverloadedStrings #-}

module Text.LaTeX.Packages.TikZ.Custom
  ( NodeProperty (..)
  , TPoint
  , pointAt , pointAtXY , pointAtXYZ
  , relPoint , relPoint_
  , TPath (..)
  , GridOption (..)
  , Step (..)
  , startingPoint
  , lastPoint
  , (->-)
  , Parameter (..)
  , TikZColor (..)
  , Color (..)
  , Word8
  , TikZ
  , emptytikz
  , path
  , scope
  , ActionType (..)
  , (->>)
  , draw
  , fill
  , clip
  , shade
  , filldraw
  , shadedraw
  , tikzpicture
  ) where

import Text.LaTeX.Packages.TikZ.Syntax
  hiding
    ( TPath
    , TikZ
    , Start
    , Cycle
    , Line
    , Rectangle
    , Circle
    , Ellipse
    , Grid
    , Node
    , startingPoint
    , lastPoint
    , path
    , (->-)
    , emptytikz
    , scope
    , (->>)
    , draw
    , fill
    , clip
    , shade
    , filldraw
    , shadedraw
    )

import qualified Data.Sequence as S

import Text.LaTeX
import Text.LaTeX.Base.Class
import Text.LaTeX.Base.Render
import Text.LaTeX.Base.Syntax

data NodeProperty
  = NodeLeft
  | NodeRight
  | NodeCenter
  | NodeAbove
  | NodeBelow
  | NodeAboveLeft
  | NodeAboveRight
  | NodeBelowLeft
  | NodeBelowRight
  | TransformShape
  deriving ( Show )

instance Render NodeProperty where
  render NodeLeft = "left"
  render NodeRight = "right"
  render NodeCenter = "center"
  render NodeAbove = "above"
  render NodeBelow = "below"
  render NodeAboveLeft = "above left"
  render NodeAboveRight = "above right"
  render NodeBelowLeft = "below left"
  render NodeBelowRight = "below right"
  render TransformShape = "transform shape"

-- | Type for TikZ paths. Every 'TPath' has two fundamental points: the /starting point/
--   and the /last point/.
--   The starting point is set using the 'Start' constructor.
--   The last point then is modified by the other constructors.
--   Below a explanation of each one of them.
--   Note that both starting point and last point may coincide.
--   You can use the functions 'startingPoint' and 'lastPoint' to calculate them.
--   After creating a 'TPath', use 'path' to do something useful with it.
data TPath
  = Start TPoint
  | Cycle TPath
  | Line TPath TPoint
  | Rectangle TPath TPoint
  | Circle TPath Double
  | Ellipse TPath Double Double
  | Grid TPath [GridOption] TPoint
  | Node TPath [NodeProperty] LaTeX
  deriving Show

instance Render TPath where
  render (Start p) = render p
  render (Cycle p) = render p <> " -- cycle"
  render (Line p1 p2) = render p1 <> " -- " <> render p2
  render (Rectangle p1 p2) = render p1 <> " rectangle " <> render p2
  render (Circle p r) = render p <> " circle (" <> render r <> ")"
  render (Ellipse p r1 r2) = render p <> " ellipse (" <> render r1 <> " and " <> render r2 <> ")"
  render (Grid p1 [] p2) = render p1 <> " grid " <> render p2
  render (Grid p1 xs p2) = render p1 <> " grid " <> render xs <> " " <> render p2
  render (Node p xs l) = render p <> " node" <> render (TransformShape:xs) <> " " <> render (TeXBraces l)
--
-- | Calculate the starting point of a 'TPath'.
startingPoint :: TPath -> TPoint
startingPoint (Start p) = p
startingPoint (Cycle x) = startingPoint x
startingPoint (Line x _) = startingPoint x
startingPoint (Rectangle x _) = startingPoint x
startingPoint (Circle x _) = startingPoint x
startingPoint (Ellipse x _ _) = startingPoint x
startingPoint (Grid x _ _) = startingPoint x
startingPoint (Node x _ _) = startingPoint x

-- | Calculate the last point of a 'TPath'.
lastPoint :: TPath -> TPoint
lastPoint (Start p) = p
lastPoint (Cycle x) = startingPoint x
lastPoint (Line _ p) = p
lastPoint (Rectangle _ p) = p
lastPoint (Circle x _) = lastPoint x
lastPoint (Ellipse x _ _) = lastPoint x
lastPoint (Grid _ _ p) = p
lastPoint (Node x _ _) = lastPoint x

-- Path builders

-- | Alias of 'Line'.
(->-) :: TPath -> TPoint -> TPath
(->-) = Line

-- | A Ti/k/Z script.
data TikZ =
    PathAction [ActionType] TPath
  | Scope [Parameter] TikZ
  | TikZSeq (S.Seq TikZ)
    deriving Show

-- | Just an empty script.
emptytikz :: TikZ
emptytikz = TikZSeq mempty

instance Render TikZ where
  render (PathAction ts p) = "\\path" <> render ts <> " " <> render p <> " ; "
  render (Scope ps t) = "\\begin{scope}" <> render ps <> render t <> "\\end{scope}"
  render (TikZSeq ts) = foldMap render ts

-- | A path can be used in different ways.
--
-- * 'Draw': Just draw the path.
--
-- * 'Fill': Fill the area inside the path.
--
-- * 'Clip': Clean everything outside the path.
--
-- * 'Shade': Shade the area inside the path.
--
--   It is possible to stack different effects in the list.
--
--   Example of usage:
--
-- > path [Draw] $ Start (pointAtXY 0 0) ->- pointAtXY 1 1
--
--   Most common usages are exported as functions. See
--   'draw', 'fill', 'clip', 'shade', 'filldraw' and
--   'shadedraw'.
path :: [ActionType] -> TPath -> TikZ
path = PathAction

-- | Applies a scope to a Ti/k/Z script.
scope :: [Parameter] -> TikZ -> TikZ
scope = Scope

-- | Sequence two Ti/k/Z scripts.
(->>) :: TikZ -> TikZ -> TikZ
(TikZSeq s1) ->> (TikZSeq s2) = TikZSeq (s1 <> s2)
(TikZSeq s) ->> a = TikZSeq $ s S.|> a
a ->> (TikZSeq s) = TikZSeq $ a S.<| s
a ->> b = TikZSeq $ a S.<| S.singleton b

-- SUGAR

-- | Equivalent to @path [Draw]@.
draw :: TPath -> TikZ
draw = path [Draw]

-- | Equivalent to @path [Fill]@.
fill :: TPath -> TikZ
fill = path [Fill]

-- | Equivalent to @path [Clip]@.
clip :: TPath -> TikZ
clip = path [Clip]

-- | Equivalent to @path [Shade]@.
shade :: TPath -> TikZ
shade = path [Shade]

-- | Equivalent to @path [Fill,Draw]@.
filldraw :: TPath -> TikZ
filldraw = path [Fill,Draw]

-- | Equivalent to @path [Shade,Draw]@.
shadedraw :: TPath -> TikZ
shadedraw = path [Shade,Draw]

tikzpicture :: LaTeXC l => TikZ -> l
tikzpicture = fromLaTeX . TeXEnv "tikzpicture" [] . rendertex
